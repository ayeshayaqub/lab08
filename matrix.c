#include "matrix.h"

void MultiplicationMM (void *args)
{
	int i,j,k;
	struct MxM* product = (struct product*)args;

	if (product->A.columns != product->B.rows)
	{
		printf("The columns of first matrix are not equal to rows of the second matrix hence cannot multiply the two matrices.\n");
		exit(1);
	}

	for ( i=product->begin; i<product->A.rows; i+=product->increment)
		for( j=0; j<product->B.columns; ++j)
			for( k=0; k<product->A.columns; ++k)
				product->result->m[i*product->result->columns + j] += product->A.m[i*product->A.columns+k] * product->B.m[k*product->B.columns+j];

}
