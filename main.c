#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include "matrix.h"

#define MAX_THREADS 8

int main (void)
{
	srand (time(NULL));
	int MSIZE = 16;
	int range = 500;

		struct Matrix A;
		struct Matrix B;
		struct Matrix result;

		A.rows = MSIZE; 
		A.columns = MSIZE; 
		A.m = malloc(sizeof(int)*A.rows*A.columns);
		B.rows = MSIZE; 
		B.columns = MSIZE; 
		B.m = malloc(sizeof(int)*B.rows*B.columns);
		result.rows = MSIZE; 
		result.columns = MSIZE; 
		result.m = malloc(sizeof(int)*result.rows*result.columns);

		int i=0;

		for (i=0; i<A.rows*A.columns; ++i)
			A.m[i] = (((double)rand()/(double)RAND_MAX) * range);

		for (i=0; i<B.rows*B.columns; ++i)
			B.m[i] = (((double)rand()/(double)RAND_MAX) * range);

		for(i=0; i<result.rows*result.columns; i++)
			result.m[i]=0;

		struct MxM args[MAX_THREADS];
		pthread_t thread[MAX_THREADS];


		for (i=0; i<MAX_THREADS; i++)
		{
			args[i].A = A;
			args[i].B = B;
			args[i].result = &result;
			args[i].begin = i;
			args[i].increment = MAX_THREADS;
		}
	

		for (i=0; i<MAX_THREADS; i++)
			pthread_join(thread[i], NULL);

		
		printf("Multiplication Complete.\n");

	return 0;
}
