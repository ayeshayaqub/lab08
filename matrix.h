void MultiplicationMM (void *args);

struct Matrix
{
	int *m;
	int rows;
	int columns;
};

struct MxM
{
	int begin;
	int increment;
	struct Matrix A;
	struct Matrix B;
	struct Matrix* result;
};
